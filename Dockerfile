FROM php:5.6-cli

RUN apt-get upgrade & apt-get update

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN apt install git -y
